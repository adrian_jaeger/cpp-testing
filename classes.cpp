#include <iostream>
#include <cstring>

using namespace std;

// Random Class for testing
class Mem
{
private:
	// static object counter
	static int globCount;
public:
	int count;
	char name[10];

	// Constructor
	Mem(const char* givenName)
	{
		memcpy(name, givenName, strlen(givenName) +1);
		this->count = globCount++;
		cout<<"Constructor for obj number "<<this->count<<" called\n";
	}

};

// Init static counter
int Mem::globCount = 1;

// Methode to display objekt information, call by value
void readMem(Mem obj)
{
	cout<<"Objekt by value, number: "<<obj.count<<" with name: "<<obj.name<<" \n";

	strcpy(obj.name, "idiot");
}

// Overload Methode to call by reference
void readMem(Mem* obj)
{
	cout<<"Objekt by reference, number: "<<obj->count<<" with name: "<<obj->name<<" \n";

	strcpy(obj->name, "idiot");
}

int main (void)
{
	cout<<"Hello World\n";

	// Create two objets on the stack
	Mem obj1("Alfed");

	Mem obj2("Manni");

	// Create one objekt on the heap, save pointer
	Mem* obj3 = new Mem("Gustav");

	// Read all created objects and modify
	readMem(obj1);

	readMem(obj2);

	readMem(obj3);


	// Read again after modification
	readMem(obj1);

	readMem(obj2);

	readMem(obj3);

	delete obj3;
}
